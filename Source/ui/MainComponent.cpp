/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_), filePlayerGui (audio_.getFilePlayer()), filePlayerGui2 (audio_.getFilePlayer(), filePlayerGui3(audio_.getFilePlayer())
{
    setSize (500, 400);
    addAndMakeVisible(filePlayerGui);
    addAndMakeVisible(filePlayerGui2);
    addAndMakeVisible(filePlayerGui3);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    filePlayerGui.setBounds (0, 0, getWidth(), 20);
    filePlayerGui2.setBounds (0, 50, getWidth(), 20);
    filePlayerGui3.setBounds(0, 100, getWidth(), 20);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
